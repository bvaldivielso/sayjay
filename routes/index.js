
/*
 * GET home page.
 */

exports.index = function(req, res){
	res.render('index', req.context.withTitle("SayJay"));
};

exports.room = require('./room').handler;

exports.api = {};

exports.api.session = require("./api/session").handler;

exports.api.songs = {};
exports.api.songs.get = require('./api/songs/get').handler;

exports.api.rooms = {};
exports.api.rooms.getMine = require('./api/rooms/get').handler;

exports.api.users = {};
exports.api.users.avatar = require('./api/users/avatar').handler;

exports.users = {};
exports.users.login = require("./users/login").handler;
exports.users.logout = require("./users/logout").handler;

exports.rooms = {};
exports.rooms.create = require("./users/private_rooms/create").handler;
exports.rooms.view = require("./users/private_rooms/view").handler;
exports.rooms.use = require("./users/private_rooms/use").handler;
