var template_name = "room/index";
var title = "Your temporary room";

exports.handler = function(req, res){
	req.context.add("not_permanent_room", true);
	res.render(template_name, req.context.withTitle(title));
};
