var template_name = "private_rooms/view";
var title = "Your rooms";

exports.handler = function(req, res){
	res.render(template_name, req.context.withTitle(title));
};
