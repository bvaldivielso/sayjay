var title = "Your room";
var template_name = "private_rooms/index";
exports.handler = function(req, res){
	var user_id = req.session.user_data.user.user_id;
	var room_id = req.params.id;
	req.models.rooms.isUserAllowedHere(user_id, room_id, function(isAllowed){
		if (isAllowed){
			req.context.add("room", isAllowed);
			req.context.add("not_permanent_room", false);
			res.render(template_name, req.context.withTitle(title));
		}
		else {
			res.send("No funciona");
		}
	});
};
