exports.handler = function(req, res){
	var room_name = req.param("room_name");
	var user_id = req.session.user_data.user.user_id;
	console.log(user_id);
	if (room_name.length >= 1){
		req.models.rooms.create(room_name, user_id, function(err){
			if (!err){
				res.redirect("/rooms/view");
			}
			else {
				console.log(err);
				res.send("There was an error creating the room");
			}
		});
	}
	else {
		res.redirect("/rooms/view");
	}
};
