exports.handler = function(req, res){
	req.session.destroy();
	res.redirect("/");
};
