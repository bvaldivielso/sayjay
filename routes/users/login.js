exports.handler = function(req, res){
	req.authorization.authorize(req.twitterUser);
	res.redirect("/rooms/view");
};
