exports.handler = function(req, res){
	var user_id = req.session.user_data.user.user_id;
	req.models.rooms.getByUserId(user_id, function(err, data){
		res.send(data);
	});
};
