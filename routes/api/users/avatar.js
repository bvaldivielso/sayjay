exports.handler = function(req, res){
	var user = req.twitterUser;
	if (!req.session.profile_picture){
		user.getUserInfo(function(error, user_string){
			var user = JSON.parse(user_string);
			req.session.profile_picture = user.profile_image_picture;
			res.redirect(user.profile_image_url);
		});
	}
	else {
		res.redirect(req.session.profile_picture);
	}
};
