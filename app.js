
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , config = require('./config')
  , models = require('./models')
  , authentication = require('./authentication')
  , context = require("./context")
  , tauth = require('twitter-auth').init(config.CONSUMER_KEY, config.CONSUMER_SECRET, config.HOST);

var app = module.exports = express.createServer();
require('./sockets').listen(app);



// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(models.setModelsInReq)
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'your secret here' }));
  app.use(authentication.middleware);
  app.use(context.middleware);
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

tauth.listen(app);


// Routes

app.get('/', routes.index);
app.get('/room', routes.room);


app.get('/users/login', tauth.RTA, routes.users.login);
app.get("/users/logout", routes.users.logout);

app.post('/rooms/create', authentication.requireIt, routes.rooms.create);
app.get('/rooms/view', authentication.requireIt, routes.rooms.view);


app.get('/private_room/:id', authentication.requireIt, routes.rooms.use);


app.get('/api/session', routes.api.session);
app.get('/api/songs', routes.api.songs.get);
app.get('/api/my_rooms', authentication.requireIt, routes.api.rooms.getMine);
app.get('/api/avatar', tauth.RTA, routes.api.users.avatar);



app.listen(config.PORT, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});






