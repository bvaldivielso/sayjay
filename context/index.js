var BasicContext = exports.BasicContext = function(isAuthenticated, user_data){
	this.isAuthenticated = isAuthenticated;
	this.user = user_data;
};

BasicContext.prototype.add = function(key, value){
	this[key] = value;
};

BasicContext.prototype.withTitle = function(title){
	this.title = title;
	return this;
};

exports.middleware = function(req, res, next){
	var isAuthenticated = req.authorization.isAuthorized();
	if (isAuthenticated){
		req.context = new BasicContext(isAuthenticated, req.session.user_data);
	}
	else {
		req.context = new BasicContext(isAuthenticated);
	}
	next();
};
