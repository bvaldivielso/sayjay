var Authorization = exports.Authorization = function(session){
	this.session = session;
};

Authorization.prototype.authorize = function(user_data){
	user_data = user_data || {};
	this.session.isAuthorized = true;
	this.session.user_data = user_data;
};

Authorization.prototype.isAuthorized = function(){
	return this.session.isAuthorized == true;
};

Authorization.prototype.get = Authorization.prototype.getUserSessionData = function(){
	this.session.user_data = this.session.user_data;
	return this.session.user_data;
};

Authorization.prototype.add = function(key, value){
	this.session.user_data[key] = value;
};

exports.middleware = function(req, res, next){
	req.authorization =	new exports.Authorization(req.session);
	next();
};

exports.requireIt = function(req, res, next){
	if (!req.authorization.isAuthorized()){
		res.redirect("/users/login");
	}
	else 
		next();
};
