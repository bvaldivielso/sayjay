/**
 * socket.io implementation in Lungo.JS
 *
 * @namespace LUNGO.Sugar
 * @class Socket_IO
 * @version 0.0
 *
 * @author Braulio Valdivielso Martínez < yo@brauliovaldivielso.eu > || @bvaldivielso
 */

LUNGO.Sugar.Socket_IO = (function(lng, io) {
	var _socket;
	var connect = function(url, options, fn){
		if (!options)	
			options = {};
		options["force new connection"] = true;
		_socket = io.connect(url, options);
		_socket.on("connect", fn);
	};    
	
	var on = function(event_name, callback){
		_socket.on(event_name, callback);
	};
	
	var emit = function(event_name, data){
		_socket.emit(event_name, data);
	};
	
	var disconnect = function(){
		_socket.disconnect();
	};
	
	var removeAllListeners = function(event_name){
		_socket.removeAllListeners(event_name);
	};
	
	return {
		connect : connect,
		on : on,
		emit : emit,
		disconnect : disconnect,
		removeAllListeners: removeAllListeners
	};
})(LUNGO, io);
