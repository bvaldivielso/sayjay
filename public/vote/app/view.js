App.View = (function(lng, app, undefined) {


	var song_list_markup = "\
	<li>\
		<a href='#' class='btn_song_item onright button green' data-id='{{youtube_id}}'>\
			<span class='icon star big' ></span>\
		</a>\
		<strong>{{name}}</strong>\
		<small>{{artist}} -- {{album}}</small>\
	</li>";
	lng.View.Template.create("song-list-template", song_list_markup);


	var song_info_markup = "<h1> {{name}} </h1>\
		<p> Artist: {{artist}} </p>\
		<p> Album: {{album}} </p>\
	";

	lng.View.Template.create("song-info-template", song_info_markup);


	var view = {};

	view.loadSongsIntoList = function(songs, fn){
		lng.View.Template.List.create({
			el: "#songs",
			data: songs,
			template: "song-list-template"
		}, fn);
	};
	
	
	view.displaySongInfo = function(song){
		console.log(song);
		lng.View.Template.render("#current_song", "song-info-template", song);
	};
 
 
    return view;
    

})(LUNGO, App);
