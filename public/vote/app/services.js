App.Services = (function(lng, app, io) {

	var services = {};

	services.getSongs = function(fn){
		var url = "/api/songs";
		lng.Service.get(url, {}, fn);
	};

	services.voteSong = function(song_id){
		services.getSession(function(session){
			var data = {
				song_id: song_id,
				room: app.Data.getRoomId(),
				is_permanent_room: false,
				session_id: session.id
			};
			io.emit("votation", data);
		});
	};



	services.getSongBeingPlayed = function(fn){
		io.removeAllListeners("ask_for_song_response");
		io.on("ask_for_song_response", fn);
		io.emit("ask_for_song", {
			room_id: app.Data.getRoomId(),
			is_permanent_room: false
		});
	};
	
	services.getSession = function(fn){
		var url = "/api/session";
		lng.Service.get(url, {}, fn);
	};


    return services;

})(LUNGO, App, LUNGO.Sugar.Socket_IO);
