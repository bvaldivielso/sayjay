App.Data = (function(lng, app, undefined) {

	var data = {};
	var _room_id;
	data.setRoomId = function(room_id){
		_room_id = room_id;
	};
	
	data.getRoomId = function(){
		return _room_id;	// Harcoded testing
	};
	
	function _getUrlVars() {
		var vars = {};
		var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
		    vars[key] = value;
		});
		return vars;
	}
	
	data.getRoomIdFromUrl = function(){
		var room = _getUrlVars()["room_id"];
		if (room.indexOf("#") != -1){
			room = room.substr(0, room.indexOf("#"));
		}
		return room;
	};

	
    return data;
})(LUNGO, App);
