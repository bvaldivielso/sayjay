App.Events = (function(lng, app, sio, growl) {

	lng.ready(function(){
		app.Data.setRoomId(app.Data.getRoomIdFromUrl());
		sio.connect(undefined, undefined, _setSocketEvents);
		app.Services.getSongs(function(songs){;
			app.View.loadSongsIntoList(songs);
		});
	});
	

	lng.dom(".btn_song_item").tap(function(){
		var song_id = $$(this).attr("data-id");
		app.Services.voteSong(song_id);
	});
	
	
	lng.dom("#btn_current_song").tap(function(){
		sio.connect("http://" + window.location.host, {}, function(){
			app.Services.getSongBeingPlayed(function(song){
				app.View.displaySongInfo(song);
			});
		});
	});
	
	
	
	lng.dom("a#btn_songs").tap(function(){
		sio.connect("http://" + window.location.host, {}, function(){			
			_setSocketEvents();
		});

	});
	
	
	function _setSocketEvents(){
		sio.on("votation_received", function(isDone){
			if (isDone){
				growl.show("Yeah", "Your vote was received", "check", true, 2);
			}
		});
		
		sio.on("votation_denied", function(data){
			growl.show("Ooopsssss....", data.message, "close", true, 2);
		});
	}

    return {

    }

})(LUNGO, App, LUNGO.Sugar.Socket_IO, LUNGO.Sugar.Growl);
