var UI = (function($){

	var ui = {};
	
	
	var room_list_markup = "\
		<tr>\
			<td><a href='/private_room/{{id}}'>{{name}}</a></td>\
			<td>{{created_at}}\
		</tr>";
	$.Template.create("room-list-template", room_list_markup);

	ui.fillListWithRooms = function(rooms){
		if (rooms.length == 0){
			$(".place_of_the_table").empty();
			$(".place_of_the_table").text("You don't have any room. Create one please");
		}
		else {
			$.Template.bind("#room-list", "room-list-template", rooms);
		}
	};
	
	
	$("#create_room").click(function(){
		var room_name = prompt("Room name");
		if (room_name != undefined){
			$("#room_name_input").val(room_name);
			$("#form").submit();
		}
	});
	


	return ui;
})(jQuery);
