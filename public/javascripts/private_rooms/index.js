(function(){	
	onYouTubeIframeAPIReady = function(){
		Sockets.init(UI.getRoomId(), socketsReady);
		UI.setListeners(Jukebox);
		Jukebox.init(Songs, YT_player, function(songs){
			UI.fillListWithSongs(songs);				
		});
	}

	$(document).ready(function(){
		loadYoutubeAPI();
	});
	
	function socketsReady(){
		UI.loadQRAndUrl();
		Sockets.on("votation", function(vote){
			if ((reason = Songs.isUserAbusing(vote)) == false){
				Songs.voteSongById(vote.song_id);
				UI.voteSongById(vote.song_id);
				
				Sockets.emit("votation_received", {
					voter_id: vote.voter_id
				});
			}
			else {
				Sockets.emit("votation_denied", {
					voter_id: vote.voter_id,
					reason: reason
				});
			}
		});
	};	
	
	
	function loadYoutubeAPI(){
		var tag = document.createElement('script');
		tag.src = "//www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	}

	
	
	
})();

