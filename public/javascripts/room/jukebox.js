var Jukebox = (function(){
	var jukebox = {};
	
	var _id_from_dom_for_player = "player";
	
	
	var _songs_manager, _player;
	jukebox.init = function(songs_manager, player, fn){
		_songs_manager = songs_manager;
		_player = player;
		_setPlayerMethods();
				
		
		_initAndSetPlayer(fn);
		
	};
	
	var _onNewSongCallback = function(){};
	
	jukebox.setOnNewSongCallback = function(cb){
		_onNewSongCallback = cb;
	};
	
	jukebox.next = function(){
		var new_song_id = _songs_manager.getNextSongId();
		_onNewSongCallback(_songs_manager.getSongById(new_song_id));
		_songs_manager.allowUsersOneMoreVote();
		_player.loadVideoById(new_song_id);
	};
	
	
	function _setPlayerMethods(){
		jukebox.isPlaying = _player.isPlaying;
		jukebox.pause = _player.pause;
		jukebox.play = _player.play;
		
		
		jukebox.getSongVotes = _songs_manager.getVotes;
		
	};
	
	
	function _initAndSetPlayer(fn){
		_player.init(_id_from_dom_for_player, function(){
			_player.play();
			_setSongChangerWhenSongsLoaded(fn);	
		});
	};
	
	function _setSongChangerWhenSongsLoaded(fn){
		_songs_manager.getSongs(function(songs){
			jukebox.next();
			_player.setEndOfVideoCallback(function(){
				jukebox.next();
			});
			fn(songs);
		});	
	};
	
	
	
	
	return jukebox;
})();
