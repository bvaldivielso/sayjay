var Sockets = (function(io){
	var sockets = {};
	var _socket;
	
	sockets.init = function(fn){
		_socket = io.connect("http://" + window.location.host);
		_socket.on("connect", function(){
			_socket.emit("init_room", _socket.socket.sessionid);			
			fn();
		});

	};
	
	sockets.broadcastSongInfo = function(song){
		sockets.emit("song_changed", song);
	};
	
	sockets.on = function(event, fn){
		_socket.on(event, fn);
	};
	
	sockets.emit = function(event, data, fn){
		_socket.emit(event, data, fn);
	};
	
	sockets.getRoomId = function(){
		return _socket.socket.sessionid;
	};
	
	
	return sockets;
})(io);
