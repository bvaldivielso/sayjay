var UI = (function(){
	var UI = {};
	
	
	var song_list_markup = "\
		<tr>\
			<td>{{name}}</td>\
			<td>{{artist}}</td>\
			<td>{{album}}</td>\
			<td id='vote_{{youtube_id}}'>0</td>\
		</tr>";
	$.Template.create("song-list-template", song_list_markup);
	
	UI.loadQRAndUrl = function(room_id){
		var votation_url = "http://" + window.location.host + "/vote/?room_id=" + room_id;
		$("#room_link").attr("href", votation_url).text(votation_url);	
		var qr_url = "http://api.qrserver.com/v1/create-qr-code/?size=250x250&data=" + escape(votation_url);
		
		$("#qr").attr("src", qr_url);
	};
	
	var _jukebox;
	UI.setListeners = function(jukebox){
		_jukebox = jukebox;
		_setPlayerButtonListeners(jukebox);
		_setSongInfoDisplayEventsAndSongBroadcasting(jukebox);
	};
	
	UI.fillListWithSongs = function(songs){
		$.Template.bind("#song-list", "song-list-template", songs);
	};
	
	UI.updateVotesOfSongs = function(){
		var votes = _jukebox.getSongVotes();
		for(var id in votes){
			UI.setSongVotes(id, votes[id]);
		}
	};
	
	UI.voteSongById = function(song_id){
		var number_of_votes = parseInt($("#vote_" + song_id).text());
		number_of_votes++;
		$("#vote_" + song_id).text(number_of_votes);
	};
	
	UI.setSongVotes = function(song_id, number_of_votes){
		$("#vote_" + song_id).text(number_of_votes + "");
	};
	
	function _setPlayerButtonListeners(jukebox){
		$("#btnNext").click(jukebox.next);
		$("#btnPlayAndPause").click(function(){
			if (jukebox.isPlaying()){
				jukebox.pause();
			}
			else {
				jukebox.play();
			}
		});		
	};
	
	
	function _setSongInfoDisplayEventsAndSongBroadcasting(jukebox){
		jukebox.setOnNewSongCallback(function(song){
			Sockets.broadcastSongInfo(song);
			
			// SONG DISPLAY INFO
			$(".song_info").fadeOut("slow", function(){
				$("#txt_song_name").text(song.name);
				$("#txt_artist").text(song.artist);
				$("#txt_album").text(song.album);
				$(".song_info").fadeIn("slow");
			});
			UI.updateVotesOfSongs();
		});
	};
	
	
	return UI;
})();







