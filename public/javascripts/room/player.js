var YT_player = (function(){
	var yt_player = {};
	
	var _player;
	
	yt_player.init = function(dom_id, fn){	
		_player = new YT.Player(dom_id, {
			width: 400,
			height: 400, // MINIMUM SIZE
			videoId: "AVbQo3IOC_A",
			playerVars: {
				wmode: "opaque"
			},
			events: {
				'onReady' : fn
			}
		});
	};
	
	yt_player.loadVideoById = function(id){
		_player.loadVideoById(id, function(){
			yt_player.play();
		});
	};
	
	yt_player.play = function(){
		_player.playVideo();
	};
	
	yt_player.pause = function(){
		_player.pauseVideo();
	};
	
	var _endVideoCallback;
	
	yt_player.setEndOfVideoCallback = function(fn){
		_endVideoCallback = fn;
		_player.addEventListener('onStateChange', function(event){
			if (event.data == YT.PlayerState.ENDED)
				fn(event);
		});
	};
	
	yt_player.next = function(){
		_endVideoCallback({
			data: 0, // 0 means it's ended
			target: _player
		});
	};
	
	yt_player.isPlaying = function(){
		return _player.getPlayerState() == 1; // 1 means it's playing
	};
	
	return yt_player;
})();


