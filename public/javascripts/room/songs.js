var Songs = (function(){
	var songs = {};
	
	var _songs, _votes;
	
	var _list_of_songs_played = [];
	
	songs.getSongs = function(fn){
		var url = "/api/songs";
		$.getJSON(url, {}, function(data){
			_songs = data;
			_votes = _initializeVotes(_songs);
			fn(data);
		});
	};	
	
	songs.getNextSongId = function(){
		var songs_with_maximum_votes = _getSongsWithMostVotesThatCanBeChosen(_votes);
		var new_song_id = _chooseRandomOne(songs_with_maximum_votes);
		
		_votes[new_song_id] = 0; // Set 0 again
		
		_list_of_songs_played.push(new_song_id);
		
		return new_song_id;
	};
	
	songs.voteSongById = function(id){
		if (_votes[id] != undefined){
			_votes[id] = _votes[id] + 1;
		}
	};
	
	songs.getSongById = function(id){
		for(var i = 0; i < _songs.length; i++){
			if (_songs[i].youtube_id == id)
				return _songs[i];
		}
		return undefined;
	};
	
	songs.getVotes = function(){
		return _votes;
	};
	
	var _votes_history = {};
	
	songs.allowUsersOneMoreVote = function(){
		for (var sess_id in _votes_history){
			_votes_history[sess_id].splice(0, 1); // Deleting first element in the votes array
		}
	};
	

	var vote_limit = 4;
	songs.isUserAbusing = function(vote){
		if (_getNumberOfVotesOfThisUser(vote.session_id) < vote_limit){
			if (_userHasAlreadyVotedThisSong(vote)){
				var problem = "You have already voted this song";
				return problem;
			}
			else {
				_addVoteToHistory(vote);		
				return false;
			}
		}
		else {
			return "You have already voted many times, you'll be able to vote when the next song starts";
		}
	};
	
	function _getNumberOfVotesOfThisUser(session_id){
		var number_of_votes = 0;
		if (_votes_history[session_id])
			number_of_votes = _votes_history[session_id].length;
		return number_of_votes;
	}
	
	function _userHasAlreadyVotedThisSong(vote){
		var votes_of_this_user = _votes_history[vote.session_id]
		if (votes_of_this_user != undefined){
			for(var i = 0; i < votes_of_this_user.length; i++){
				if (votes_of_this_user[i].song_id == vote.song_id)
					return true;
			}
		}
		return false;
	}
	
	function _addVoteToHistory(vote){
		var session_id = vote.session_id;
		_votes_history[session_id] = _votes_history[session_id] || [];
		_votes_history[session_id].push(vote);		
	}
	
	function _initializeVotes(songs){
		var votes = {};
		for(var i = 0; i < songs.length; i++){
			votes[songs[i].youtube_id] = 0; // Using youtube_id as the song identificator
		}
		return votes;
	};
	
	function _getSongsWithMostVotesThatCanBeChosen(songs){
		var _songs = jQuery.extend({}, songs); // Copy object
		_songs = _removeSongsThatWhereRecentlyPlayed(_songs);
		return _getSongsWithMostVotes(_songs);
	};
	
	function _removeSongsThatWhereRecentlyPlayed(songs){
		for (var song_id in songs){
			if (_wasRecentlyPlayed(song_id))
				delete songs[song_id];
		}
		return songs;
	};
	
	function _wasRecentlyPlayed(song_id){
		var how_far_is_recently = 3;
		var top_of_list_array = _list_of_songs_played.length;
		for (var i = top_of_list_array - how_far_is_recently; i < top_of_list_array; i++){
				if (song_id == _list_of_songs_played[i]){
					return true;
				}
		}
		return false;
	};
	
	function _getSongsWithMostVotes(songs){
		var maximum_votes = 0;
		var songs_with_maximum_votes = [];
		for(var id in songs){
			if (songs[id] == maximum_votes)
				songs_with_maximum_votes.push(id);
			else if (songs[id] > maximum_votes){
				maximum_votes = songs[id];
				songs_with_maximum_votes = [id];
			}
		}
		return songs_with_maximum_votes;
	} 	
	
	function _chooseRandomOne(array){
		return array[Math.floor(Math.random()*array.length)];
	};
	
	return songs;
})();


