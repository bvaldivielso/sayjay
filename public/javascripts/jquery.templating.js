/*     Based on LungoJS templating system (http://lungojs.com)
 *     License : GPLv3
 *      
 *     Simple Template Manager Plugin for JQuery 
 *     Also noun as "El mataviejas" (We don't know why)
 *     
 *     
 *     You can find how to use it at  README
 *
 */

(function($) {
 $.Template = function() {};   
 $.Template.created_templates = {}; 
 /**
 * Creates a new template. 
 *
 * @method create
 *
 * @param template_name {string} Template name
 * @param template_markup {string} Template markup code
 **/
  $.Template.create = function(template_name, template_markup){
      $.Template.created_templates[template_name] = template_markup;
  }
  
  
  /**
 * Bind data to a created template 
 *
 * @method bind
 *
 * @param selector {string} A CSS-like container selector (#myDiv, .myClass, ...)
 * @param template_name {string} Name of the template you wanna use
 * @param data {object/object array} Which contains the data which you wanna bind to the template
 **/
  $.Template.bind = function(selector, template_name, data){
      if (data.length == undefined){
          data = [ data ];
      }
      for (var i = 0; i < data.length; i++)
          {
              var current_data = data[i];
              var current_markup = $.Template.created_templates[template_name];
              for (var key in current_data)
                  {
                      while (current_markup.search("{{"+key+"}}") != -1)
                          {
                              current_markup = current_markup.replace("{{" + key + "}}", current_data[key]);
                          }                                
                  }
              $(selector).append(current_markup);
          }
          
  }
  

})(jQuery); 
