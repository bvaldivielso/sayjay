var models = exports.models = {
	rooms : require('./rooms')
};

exports.setModelsInReq = function(req, res, next){
	req.models = models;
	next();
};

