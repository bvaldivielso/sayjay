var table_name = "rooms";
var client = require('../database-connection').client;

var create = exports.create = function(name, creator_id, fn){
	var insert = {
		name: name,
		creator: creator_id
	};
	
	client.insert(table_name, insert, fn);
	
};

var getByUserId = exports.getByUserId = function(user_id, fn){
	var data = {
		creator: user_id
	};
	
	client.select(table_name, { where: data }, fn);
};


var isUserAllowedInHere = exports.isUserAllowedHere = function(user_id, room_id, fn){
	var data = {
		creator: user_id,
		id: room_id
	};
	
	client.select(table_name, { where : data }, function(err, data){
		if (data.length == 1){
			fn(data[0]);
		}
		else {
			fn(false);
		}
	});	
};

