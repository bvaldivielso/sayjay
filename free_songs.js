exports.songs = [
	{
		artist: "Yris",
		name: "L'abbaye",
		album: "Waltz of summer green",
		youtube_id: "KuhNOm81ljY"
	},
	{
		artist: "Beat Six",
		name: "Little Mess",
		album: "Beatsix EP",
		youtube_id: "6ZoEKvNDrnE"
	},
	{
		artist: "No Control",
		name: "Justep",
		album: "Critical Situation",
		youtube_id: "zCc9ZGU0vas"
	},
	{
		artist: "Soundstatues",
		name: "Give it up",
		album: "Soundstatues",
		youtube_id: "QPnClxj5alw"
	},
	{
		artist: "Basement Skylights",
		name: "KillingFields",
		album: "Basement Skylights",
		youtube_id: "aNeCUmXlEmM"
	},
	{
		artist: "Ay-14ice",
		name: "Don't follow me",
		album: "Show me what you got",
		youtube_id: "cbu4xdPyhao"
	},
	{
		artist: "Manolis Moumouzias",
		name: "In Love",
		album: "Desert of silence",
		youtube_id: "lXI-riUVGrM"
	},
	{
		artist: "Sean Fournier",
		name: "King of the World",
		album: "Sean Fournier",
		youtube_id: "s_sBe6MVPDo"
	},
	{
		artist: "Jonathan Dimmel",
		name: "You've got it",
		album: "The worst and best",
		youtube_id: "-2HvDIEWuh0"
	}, 
	{
		artist: "Fhernando",
		name: "Ride My Tempo",
		album: "Last Days of Disco",
		youtube_id: "PnK1INZLBSs"
	},
	{
		artist: "The House of the Old Boat",
		name: "Picadilly",
		album: "We didn't mean to go to sea LP",
		youtube_id: "y2ZhGnytcPs"
	},
	{
		artist: "Thisan",
		name: "Misteris",
		album: "Gotas de néctar",
		youtube_id: "XAs2J06vzdg"
	},
	{
		artist: "Emerald Park",
		name: "At The Mall",
		album: "For Tomorrow",
		youtube_id: "41jmaUB6lAo"
	},
	{
		artist: "Slikk tim",
		name: "Bahia",
		album: "Gutter Guitar",
		youtube_id: "hvTxTrjdc8Q"
	},
	{
		artist: "StatueOfDiveo",
		name: "Whatever you want",
		album: "Whatever you want",
		youtube_id: "-J_XfdISlb4"	
	},
	{
		artist: "Heifervescent",
		name: "The Great Collapsing Circus",
		album: "Little Egg",
		youtube_id: "XIhaZ8NrjsE"
	}
];
