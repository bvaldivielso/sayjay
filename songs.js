exports.songs = [
	{
		artist: "Carla Rae Jebsen",
		name: "Call me maybe",
		album: "Curiosity",
		youtube_id: "fWNaR-rxAic"
	},
	{
		artist: "Justin Bieber",
		name: "Boyfriend",
		album: "Believe",
		youtube_id: "4GuqB1BQVr4"
	},
	{
		artist: "Katy Perry",
		name: "Wide awake",
		album: "Teenage Dream: The Complete Confection",
		youtube_id: "k0BWlvnBmIE"
	},
	{
		artist: "Drake feat. Lil Wayne",
		name: "HYFR",
		album: "Take care",
		youtube_id: "4Xn9zStyKe8"
	},
	{
		artist: "Nach",
		name: "Humano ser",
		album: "Mejor que el silencio",
		youtube_id: "bpvQpIkCTnQ"
	},
	{
		artist: "Skrillex",
		name: "Bangarang",
		album: "Bangarang",
		youtube_id: "YJVmu6yttiw"
	},
	{
		artist: "AC/DC",
		name: "Back in Black",
		album: "Back in Black",
		youtube_id: "tBQ0_9IFzU0"
	},
	{
		artist: "Pantless Knights",
		name: "The New Dork",
		album: "Entrepreneur State of Mind",
		youtube_id: "exmwSxv7XJI"
	},
	{
		artist: "Pantless Knights",
		name: "We all are Steve",
		album: "Steve Jobs Tribute",
		youtube_id: "ia4joyGYG_A"
	},
	{
		artist: "Maroon 5 ft. Wiz Khalifa",
		name: "Payphone",
		album: "Overexposed",
		youtube_id: "5FlQSQuv_mg"
	},
	{
		artist: "Travis McCoy ft. Bruno Mars",
		name: "Billionaire",
		album: "Lazarus",
		youtube_id: "8aRor905cCw"
	},
	{
		artist: "David Guetta ft. Sia",
		name: "Titanium",
		album: "Nothing But the Beat",
		youtube_id: "JRfuAukYTKg"
	},
	{
		artist: "Queen",
		name: "Bohemian Rhapsody",
		album: "A Night at The Opera",
		youtube_id: "irp8CNj9qBI"
	},
	{
		artist: "John Coltrane",
		name: "My Favorite Things",
		album: "My Favorite Things",
		youtube_id: "qWG2dsXV5HI"
	},
	{
		artist: "The White Stripes",
		name: "Seven Nation Army",
		album: "Elephant",
		youtube_id: "0J2QdDbelmY"
	},
	{
		artist: "Bruno Mars",
		name: "Grenade",
		album: "Doo-Wops & Hooligans",
		youtube_id: "SR6iYWJxHqs"
	},
	{
		artist: "Red Hot Chilli Peppers",
		name: "Californication",
		album: "Californication",
		youtube_id: "YlUKcNNmywk"
	},
	{
		artist: "Coldplay",
		name: "Viva la Vida",
		album: "Mylo Xyloto",
		youtube_id: "dvgZkm1xWPE"
	},
	{
		artist: "Kiss",
		name: "Rock n' Roll All Nite",
		album: "Dressed to Kill",
		youtube_id: "EFMD7Usflbg"
	}
];





