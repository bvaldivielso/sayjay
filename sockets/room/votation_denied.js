exports.handler = function(io, socket){
	var callback = function(data){
		var voter_id = data.voter_id;
		io.sockets.socket(voter_id).emit("votation_denied", { message: data.reason });
	};
	return callback;
};
