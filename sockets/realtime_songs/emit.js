var songs = require("./index");
var identify_socket = require("../identify_sockets");

exports.handler = function(io, socket){
	var callback = function(data){
		var room_socket_id = identify_socket.getSocketIdByRoomIdAndPermanentialBoolean(data.is_permanent_room, data.room_id);
		var current_song = songs.getSongPlayedInRoom(room_socket_id);
		socket.emit("ask_for_song_response", current_song);
	};
	return callback;
};
