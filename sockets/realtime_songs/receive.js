


var songs = {};


exports.handler = function(io, socket){
	var callback = function(song){
		var room = socket.id; // You know, socket.id is also the id of the room
		songs[room] = song;		
	};
	
	return callback;
};

exports.getSongPlayedInRoom = function(room_id){
	return songs[room_id];
};
