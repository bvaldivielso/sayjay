var identify_sockets = require("../identify_sockets"); 

var gSIBRIAPB = identify_sockets.getSocketIdByRoomIdAndPermanentialBoolean;

exports.handler = function(io, socket){
	var callback = function(votation){
		var song_id = votation.song_id;
		var voter_id = socket.id;
		
		console.log(votation);
		
		var recipient_socket_id =  gSIBRIAPB(votation.is_permanent_room, votation.room);
		
		console.log(recipient_socket_id);
		
		if (__socketExists(io, recipient_socket_id)){
			io.sockets.socket(recipient_socket_id).emit("votation", {
				song_id: song_id,
				voter_id: voter_id,
				session_id: votation.session_id
			});		
		}
		else {
			console.log("There was this problem");
			__sendVotationDenied(socket);
		}		
	};
	return callback;
};


function __socketExists(io, socket_id){
	if (socket_id != undefined){
		return true;
	}
	else {
		return false;
	}
};

function __sendVotationDenied(socket){
	socket.emit("votation_denied", { message: "This room seem not to be being used right now" });
};
