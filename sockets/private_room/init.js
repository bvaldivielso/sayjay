var identify_sockets = require("../identify_sockets");
exports.handler = function(socket){
	var callback = function(data){
		identify_sockets.associateSocketIdToRoom(data.room_id, data.socket_id);
		socket.on("disconnect", function(){
			identify_sockets.eliminateSocket(socket.id);
		});
	};
	
	return callback;
};


