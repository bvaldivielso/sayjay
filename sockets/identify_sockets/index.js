var __rooms = {};

exports.associateSocketIdToRoom = function(room_id, socket_id){
	__rooms[room_id] = socket_id;
};

exports.eliminateSocket = function(id){
	__rooms[id] = undefined;
};

exports.getSocketIdByRoomId = function(room_id){
	return __rooms[room_id];
};

exports.getSocketIdByRoomIdAndPermanentialBoolean = function(is_permanent, room_id){
	if (is_permanent)
		return exports.getSocketIdByRoomId(room_id);
	else 
		return room_id;
};
