var room = require("./room");
var voters = require("./voters");
var songs = require("./realtime_songs");
var private_room = require("./private_room");

exports.listen = function(app){
	var io = require('socket.io').listen(app);
	
	io.sockets.on("connection", function(socket){
		socket.on("init_room", room.init(socket));
		socket.on("votation", voters.votation(io, socket));
		socket.on("votation_received", room.votation_received(io, socket));
		socket.on("votation_denied", room.votation_denied(io, socket));
		
		
		socket.on("init_room_permanent", private_room.init(socket));
		
		
		socket.on("song_changed", songs.receiveSongInfo(io, socket));
		socket.on("ask_for_song", songs.sendSongInfo(io, socket));
	});	
};
